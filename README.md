# Internet Web and Cloud Course
## Fall 2020

**Hello, Welcome!** 

**_This is my first project experimenting with markdown syntax which includes the following_**
 
 - Unordered item
 
 1. Ordered item
 
 > Blockquotes
 >
 >> Nested Blockquote
 
 _italic text_
 
 **Bold Text**
 
 This is a paragraph
 
 # Heading Level 1
 
 ## Heading Level 2
 
 ### Heading Level 3
 
 ##### Heading Level 4

My favorite Website is [Facebook](https://www.facebook.com).

Email me at <gootyag@pdx.edu>

