"""
A simple web application for listing charities and social services in Portland app.
"""

#Importing from Form, Index and Listing
import flask
from flask.views import MethodView
from form import Form  # Form presenter for rendering the form page to enter the charity details and to post the listings to the model to insert in database
from listing import Listing # Listing presenter for quering all entries from database and display it to the listing.html
from index import Index # Index presenter for rendering landing page
from photo import Photo

#Flask app
app = flask.Flask(__name__)

#End point for the index page. Index can be accessed with / in the URL
app.add_url_rule('/',
                  view_func=Index.as_view('index'),
                  methods=['GET'])

#End point for the form page. Form cam be accessed with the /form/ in the URL
app.add_url_rule('/form/',
                 view_func=Form.as_view('form'),
                 methods=['GET', 'POST'])

#End point for the listing page. Listing can be accessed with the /listing/ in the URL
app.add_url_rule('/listing/',
                 view_func=Listing.as_view('listing'),
                methods=['GET'])

#End point for the photo page. Form cam be accessed with the /photo in the URL
app.add_url_rule('/photo',
                 view_func=Photo.as_view('photo'),
                 methods=['GET','POST'])

if __name__ == '__main__':
    #hostname and port information on which the web application runss
    app.run(host='0.0.0.0', port=5000, debug=True)

