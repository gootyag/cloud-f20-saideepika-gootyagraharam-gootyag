"""
This is the python file for Form
"""
from flask import redirect, request, url_for, render_template
from google.cloud import translate_v2 as translate
from flask.views import MethodView
import gbmodel
import six

#This helps in renders the form.html page
class Form(MethodView):
    def get(self):
        return render_template('form.html')

#POST Method
    def post(self):

        """
        Accepts POST requests, and processes the form;
        """
#Searches for the model or backend and inserts the values entered in the form to the backend
#This includes values of name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, and media

        model = gbmodel.get_model()
        dict = {}
        var = ''

        translate_client = translate.Client()
        #translate each entry to english
        for item in request.form:
            var = request.form[item]
            if isinstance(request.form[item], six.binary_type):
                var  = request.form[item].decode('utf-8')
            dict[item] = translate_client.translate(var,target_language='en') #dictionnary of translated items

        model.insert(dict['name']['translatedText'], dict['location']['translatedText'], dict['country']['translatedText'], dict['travel_date']['translatedText'], dict['travel_days']['translatedText'], dict['purpose']['translatedText'], dict['rating']['translatedText'], dict['review']['translatedText'], dict['media']['translatedText'])

        """
       After Filling the form, it redirects to form page and a new form is displayed
        """
#Given a link to redirect to the same form.html submitting the form. When user submits the form, a new form with empty fields is displayed
        return redirect(url_for('form'))

