"""
This is the Model.py file with the class Model
"""
class Model():
    def select(self):

        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, location, country, travel_date, travel_days, purpose, rating, review, media):
        
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param location: String
        :param country: String
        :param travel_date: Date
        :param travel_days: int
        :param purpose: String
        :param rating: String
        :param review: String
        :param media: Blob/varbinary
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
