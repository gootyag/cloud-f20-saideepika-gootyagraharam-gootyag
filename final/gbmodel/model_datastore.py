# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Datastore Model
"""
#This is the model_datastore.py file
from .Model import Model
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.
    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]
    This returns:
        [ name, location, country, travel_date, travel_days, purpose, rating, review, media ]
    where name, location, country, purpose, rating, review, media are Python strings, travel_date is date, travel_days is int and media is blob 
    """

    #If there is no entity then return none, otherwise return the entity values
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'], entity['location'], entity['country'], entity['travel_date'], entity['travel_days'], entity['purpose'], entity['rating'], entity['review'], entity['media']]

#This is the class Model
class model(Model):
    def __init__(self):
#Client her is the id of the project in the GCP
        self.client = datastore.Client('noted-casing-291400')

#Here the Kind type is Show. Entities fetches the data from the database using the select query

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, location, country, travel_date, travel_days, purpose, rating, review, media
        :return: List of lists containing all rows of database
        """
        query = self.client.query(kind = 'ftravel')
        entities = list(map(from_datastore,query.fetch()))
        return entities

#Datastore values are inserted into the database with the insert command with the key as show
    def insert(self, name, location, country, travel_date, travel_days, purpose, rating, review, media):
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param location: String
        :param country: String
        :param travel_date: Date
        :param travel_days: int
        :param purpose: String
        :param rating: String
        :param review: String
        :param media: Blob/varbinary
        :return: none
        :raises: Database errors on connection and insertion
        """
        key = self.client.key('ftravel')
        rev = datastore.Entity(key)
        rev.update( {
            'name': name,
            'location' : location,
            'country' : country,
            'travel_date' : travel_date,
            'travel_days' : travel_days,
            'purpose' : purpose,
            'rating' : rating, 
            'review' : review,
            'media' : media
            })
        self.client.put(rev)
        return True
