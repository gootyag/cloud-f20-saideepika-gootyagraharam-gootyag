# """
# sqlite Model
# """
# """
# A simple web application named "Hope" for listing charities and social services in Portland app.

# Data is stored in a SQLite database table named "user" that looks something like the following:

# +-------------------------+--------------------------------+-----------------+----------------+----------------+-------------+---------------+--------------------+-----------------------+
# | Name                    |            Description         | Street Address  | Type of Service| Beneficiaries  |     Date    |  Phone Number | Hours of Operation | Additional Attachment |
# +================+========================+=================+================+=============+========================+=====================+===============================================+
# | Abc Books Distributors  |            Donate books        | 1604 SW Clay St |   Eductation   | General Public |  2020-10-23 |   9715xxxxxx  |         5          |        image.jpg      |
# +-------------------------+--------------------------------+-----------------+----------------+----------------+-------------+---------------+--------------------+-----------------------+

# This can be created with the following SQL (see bottom of this file):

# create table travel (name varchar, description varchar, street_address varchar, service_type varchar, beneficiaries varchar, date date, phone_number varchar, hours int, reviews varchar, media varbinary);

# """
# from datetime import date
# from .Model import Model
# import sqlite3
# DB_FILE = 'entries.db'    # file for our Database

# class model(Model):
#     def __init__(self):
#         # Make sure our database exists
#         connection = sqlite3.connect(DB_FILE)
#         cursor = connection.cursor()
#         try:
#             cursor.execute("select count(rowid) from travel")
#         except sqlite3.OperationalError:
#             cursor.execute("create table travel (name varchar, location varchar, country varchar, travel_date date, travel_days int, purpose varchar, rating int, review varchar, media varbinary)")  #Create travel table if not already present
#         cursor.close()

#     def select(self):
#         """
#         Gets all rows from the database
#         Each row contains: name, location, country, travel_date, travel_days, purpose, rating, review, media
#         :return: List of lists containing all rows of database
#         """
#         connection = sqlite3.connect(DB_FILE)
#         cursor = connection.cursor()
#         cursor.execute("SELECT * FROM travel")
#         return cursor.fetchall()

#     def insert(self, name, location, country, travel_date, travel_days, purpose, rating, review, media):
#         """
#         Inserts entry into database
#         :param self: String
#         :param name: String
#         :param location: String
#         :param country: String
#         :param travel_date: Date
#         :param travel_days: int
#         :param purpose: String
#         :param rating: String
#         :param review: String
#         :param media: Blob/varbinary
#         :return: none
#         :raises: Database errors on connection and insertion
#         """
#         params = {'name':name, 'location':location, 'country':country, 'travel_date':travel_date, 'travel_days':travel_days, 'purpose':purpose, 'rating':rating, 'review':review, 'media':media}
#         connection = sqlite3.connect(DB_FILE)
#         cursor = connection.cursor()
#         cursor.execute("insert into travel (name, location, country, travel_date, travel_days, purpose, rating, review, media) VALUES (:name, :location, :country, :travel_date, :travel_days, :purpose, :rating, :review, :media)", params)

#         connection.commit()
#         cursor.close()
#         return True
