"""
This is the python file for Index
"""

#This is index.py page which simply renders the index.html upon redirection

from flask import render_template
from flask.views import MethodView
import gbmodel
import os

#GET method
#This sets the model and renders the index.html page
class Index(MethodView):
        def get(self):
                model = gbmodel.get_model()
                return render_template('index.html', landmark={})