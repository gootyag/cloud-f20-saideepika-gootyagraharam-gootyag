
"""
This is the python file for Listing
"""

#This is the listing.py page which retieves the values from the database and displays it in the listing.html page

from flask import render_template
from flask.views import MethodView
import gbmodel
import os

#GET method
#This gets the model and renders the lsiting.html page with all the listings made by the users
#The listings include details about name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media
class Listing(MethodView):
    def get(self):

        model = gbmodel.get_model()
        entries = [dict(name=row[0], location=row[1], country=row[2], travel_date=row[3], travel_days=row[4], purpose=row[5], rating=row[6], review=row[7], media=row[8]) for row in model.select()]
        return render_template('listing.html',entries=entries)