from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel
import os
import six
from google.cloud import storage
from google.cloud import vision
CLOUD_STORAGE_BUCKET = 'gootyag_travel_bucket'
class Photo(MethodView):
    def get(self):
        """
        To render landing page to view
        """
        return render_template('index.html')

    def post(self):
        """
        Accepts image POST requests, and processes the form;
        Renders the landmark details of the image to the index view
        """
        photo = request.files['file']

        # Create a Cloud Storage client.
        storage_client = storage.Client()

        # Get the bucket that the file will be uploaded to.
        bucket = storage_client.get_bucket(CLOUD_STORAGE_BUCKET)

        # Create a new blob and upload the file's content.
        blob = bucket.blob(photo.filename)
        blob.upload_from_string(photo.read(), content_type=photo.content_type)

        # Make the blob publicly viewable.
        blob.make_public()
        # Create a Cloud Vision client.
        vision_client = vision.ImageAnnotatorClient()

        # Use the Cloud Vision client to detect landmark for the image uploaded.
        source_uri = 'gs://{}/{}'.format(CLOUD_STORAGE_BUCKET, blob.name)

        image = vision.Image(source=vision.ImageSource(gcs_image_uri=source_uri))
       #Get landmark details of the image
        response = vision_client.landmark_detection(image=image)
        labels = response.landmark_annotations
        #Display message if image landmark is not detected
        landmark = {"description" : "Could not identify landmark"}
        if labels:
           landmark = {"description" : labels[0].description, "latitude" : labels[0].locations[0].lat_lng.latitude, "longitude": labels[0].locations[0].lat_lng.longitude}
        return render_template('index.html', landmark=landmark)