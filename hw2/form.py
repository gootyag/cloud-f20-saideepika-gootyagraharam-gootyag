from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Form(MethodView):
    def get(self):
        return render_template('form.html')

    def post(self):

        """
        Accepts POST requests, and processes the form;
        """
        model = gbmodel.get_model()
        model.insert(request.form['name'], request.form['description'], request.form['street_address'], request.form['service_type'],  request.form['beneficiaries'], request.form['date'], request.form['phone_number'], request.form['hours'], request.form['reviews'], request.form['media'] )

        """
       After Filling the form, it redirects to form page and a new form is displayed
        """
        return redirect(url_for('form'))


