"""
A simple web application for listing charities and social services in Portland app.
"""
import flask
from flask.views import MethodView
from form import Form
from listing import Listing
from index import Index

app = flask.Flask(__name__)       # our Flask app

#End points for different pages

app.add_url_rule('/',
                  view_func=Index.as_view('index'),
                  methods=['GET'])

app.add_url_rule('/form/',
                 view_func=Form.as_view('form'),
                 methods=['GET', 'POST'])

app.add_url_rule('/listing/',
                 view_func=Listing.as_view('listing'),
                methods=['GET'])


#hostname and port information on which the web application runs


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

