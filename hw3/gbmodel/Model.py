class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, description, street_address, service_type, phone_number, hours, reviews, media):
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param description: String
        :param street_address: String
        :param service_type: String
        :param phone_number: String
        :param hours: int
        :param reviews: String
        :param media: Blob/varbinary
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
