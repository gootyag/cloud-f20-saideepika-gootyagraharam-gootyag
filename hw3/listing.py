#This is the listing.py page which retieves the values from the database and displays it in the listing.html page

from flask import render_template
from flask.views import MethodView
import gbmodel

class Listing(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], description=row[1], street_address=row[2], service_type=row[3], beneficiaries=row[4], date=row[5], phone_number=row[6], hours=row[7], reviews=row[8], media=row[9]) for row in model.select()]
        return render_template('listing.html',entries=entries)