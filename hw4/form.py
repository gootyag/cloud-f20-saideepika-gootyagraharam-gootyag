
"""
This is the python file for Form 
"""
from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

#This helps in renders the form.html page
class Form(MethodView):
    def get(self):
        return render_template('form.html')

#POST Method
    def post(self):

        """
        Accepts POST requests, and processes the form;
        """
#Searches for the model or backend and inserts the values entered in the form to the backend
#This includes values of name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, and media

        model = gbmodel.get_model()
        model.insert(request.form['name'], request.form['description'], request.form['street_address'], request.form['service_type'],  request.form['beneficiaries'], request.form['date'], request.form['phone_number'], request.form['hours'], request.form['reviews'], request.form['media'] )

        """
       After Filling the form, it redirects to form page and a new form is displayed
        """
#Given a link to redirect to the same form.html submitting the form. When user submits the form, a new form with empty fields is displayed
        return redirect(url_for('form'))


