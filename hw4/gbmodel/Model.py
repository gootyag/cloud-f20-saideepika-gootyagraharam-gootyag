"""
This is the Model.py file with the class Model
"""
class Model():
    def select(self):

        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

"""
Here we have all the table attributes
"""
    def insert(self, name, description, street_address, beneficiaries, date, service_type, phone_number, hours, reviews, media):
        
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param description: String
        :param street_address: String
        :param service_type: String
        :param phone_number: String
        :param hours: int
        :param reviews: String
        :param media: Blob/varbinary
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
