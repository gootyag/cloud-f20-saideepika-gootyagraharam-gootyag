
"""
Choosing Backend for the application
"""

"""
Choosing the datastore as the backend for this assignment
"""
model_backend = 'datastore'
#model_backend = 'pylist'
#model_backend = 'sqlite3'

#If the backend is sqlite3, import the sqlite3 model
if model_backend == 'sqlite3':
    from .model_sqlite3 import model
#If the backend is pylist, import the pylist model
elif model_backend == 'pylist':
	from .model_pylist import model
#If the backend is datastore, import the datastore model
elif model_backend == 'datastore':
    from .model_datastore import model
#If no backend is found, Show the output as no appropriate databackend configured
else:
    raise ValueError("No appropriate databackend configured. ")

"""
The backend model is stored in the appmodel
"""
appmodel = model()

#get model
def get_model():

"""
returming the contents of the appmodel
"""
    return appmodel
