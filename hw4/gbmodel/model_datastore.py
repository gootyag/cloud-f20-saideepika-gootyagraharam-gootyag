# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Datastore Model
"""
#This is the model_datastore.py file
from .Model import Model
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.
    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]
    This returns:
        [ name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media ]
    where name, description, street_address, service_type, beneficiaries, date, phone_number, reviews are Python strings, hours is int and media is blob 
    """

    #If there is no entity then return none, otherwise return the entity values
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'], entity['description'], entity['street_address'], entity['service_type'], entity['beneficiaries'], entity['date'], entity['phone_number'], entity['hours'], entity['reviews'], entity['media']]

#This is the class Model
class model(Model):
    def __init__(self):
#Client her is the id of the project in the GCP
        self.client = datastore.Client('noted-casing-291400')

#Here the Kind type is Show. Entities fetches the data from the database using the select query

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media
        :return: List of lists containing all rows of database
        """
        query = self.client.query(kind = 'Show')
        entities = list(map(from_datastore,query.fetch()))
        return entities

#Datastore values are inserted into the database with the insert command with the key as show
    def insert(self, name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media):
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param description: String
        :param street_address: String
        :param service_type: String
        :param beneficiaries: String
        :param date: Date
        :param phone_number: int
        :param hours: int
        :param reviews: String
        :param media: Blob/varbinary
        :return: True
        :raises: Database errors on connection and insertion
        """
        key = self.client.key('Show')
        rev = datastore.Entity(key)
        rev.update( {
            'name': name,
            'description' : description,
            'street_address' : street_address,
            'service_type' : service_type,
            'beneficiaries' : beneficiaries,
            'date' : date,
            'phone_number' : phone_number, 
            'hours' : hours,
            'reviews' : reviews,
            'media' : media
            })
        self.client.put(rev)
        return True