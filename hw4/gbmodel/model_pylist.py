"""
Pylist Model
"""
"""
This is the model_pylist.py file
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.user = []

    def select(self):
        """
        Returns charities list of lists
        Each row contains: name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media
        :return: List of lists
        """
        return self.user

    def insert(self, name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media):
        """
        Appends a new list of values representing new message into user
        :param self: String
        :param name: String
        :param description: String
        :param street_address: String
        :param service_type: String
        :param beneficiaries: String
        :param date: Date
        :param phone_number: int
        :param hours: int
        :param reviews: String
        :param media: Blob/varbinary
        :return: True
        """
        params = [name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media]
        self.user.append(params)
        return True