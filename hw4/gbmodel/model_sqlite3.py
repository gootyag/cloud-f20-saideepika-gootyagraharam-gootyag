"""
sqlite Model
"""
"""
A simple web application named "Hope" for listing charities and social services in Portland app.

Data is stored in a SQLite database table named "user" that looks something like the following:

+-------------------------+--------------------------------+-----------------+----------------+----------------+-------------+---------------+--------------------+-----------------------+
| Name                    |            Description         | Street Address  | Type of Service| Beneficiaries  |     Date    |  Phone Number | Hours of Operation | Additional Attachment |
+================+========================+=================+================+=============+========================+=====================+===============================================+
| Abc Books Distributors  |            Donate books        | 1604 SW Clay St |   Eductation   | General Public |  2020-10-23 |   9715xxxxxx  |         5          |        image.jpg      |
+-------------------------+--------------------------------+-----------------+----------------+----------------+-------------+---------------+--------------------+-----------------------+

This can be created with the following SQL (see bottom of this file):

create table user (name varchar, description varchar, street_address varchar, service_type varchar, beneficiaries varchar, date date, phone_number varchar, hours int, reviews varchar, media varbinary);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from user")
        except sqlite3.OperationalError:
            cursor.execute("create table user (name varchar, description varchar, street_address varchar, service_type varchar, beneficiaries varchar, date date, phone_number varchar, hours int, reviews varchar, media varbinary)")  #Create user table if not already present
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM user")
        return cursor.fetchall()

    def insert(self, name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media):
        """
        Inserts entry into database
        :param self: String
        :param name: String
        :param description: String
        :param street_address: String
        :param service_type: String
        :param beneficiaries: String
        :param date: Date
        :param phone_number: int
        :param hours: int
        :param reviews: String
        :param media: Blob/varbinary
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'description':description, 'street_address':street_address, 'service_type':service_type, 'beneficiaries':beneficiaries, 'date':date, 'phone_number':phone_number, 'hours':hours, 'reviews':reviews, 'media': media}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into user (name, description, street_address, service_type, beneficiaries, date, phone_number, hours, reviews, media) VALUES (:name, :description, :street_address, :service_type, :beneficiaries, :date, :phone_number, :hours, :reviews, :media)", params)

        connection.commit()
        cursor.close()
        return True
